var mongoose = require('mongoose');
mongoose.set('debug', true);

var PostSchema = new mongoose.Schema({
	headline: String,
	body: String,
	author: String,
	upvotes: {type: Number, default:0},
	created_at: Number,
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});

PostSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Post', PostSchema);