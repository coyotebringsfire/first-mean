var app = angular.module('flapperNews', ['ui.router']);

app.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
	        url: '/home',
	        templateUrl: '/home.html',
	        controller: 'MainController',
	        resolve: {
              postPromise: ['posts', function(posts){
                return posts.getAll();
              }]
            }
        });

    $urlRouterProvider.otherwise('home');
}]);

app.factory('posts', [ '$http', function($http){
	var o = {
    	posts: []
	};
	o.getAll = function() {		
	    var httpObj=$http.get('/posts');
	    
	    httpObj.success(function(data){
          console.log("all posts", data);
	      //convert the created_at for each post to a string
	      angular.forEach(data, function(data_post, key) {	
	      	data_post.created_string=new Date(data[key].created_at).toDateString();
	      	data_post.addComment=function(comment) {
	      	  console.log(o.posts);
          	  console.log("adding comment", data_post);
          	  data[key].comments.push({
          	  	body: comment.body,
          	  	author:"loser",
          	  	upvotes:0
          	  });
          	  console.log(data_post);
            };
          });
        });
    	httpObj.error(function(data, status, headers, config) {
	     // called asynchronously if an error occurs
	     // or server returns response with an error status.
	     alert("Error");
	     console.log("ERROR getting posts", data, status, headers);
	    });
	    return httpObj;
    };
    o.create = function(post) {
    	console.log("creating post", post);
      return $http.post('/posts', post).success(function(data){
      	console.log("success");
        o.posts.push(data);
      })
      .error(function(data, status, headers, config) {
	     // called asynchronously if an error occurs
	     // or server returns response with an error status.
	     alert("Error");
	     console.log("ERROR saving post", data, status, headers);
	    });
    };
    o.upvote = function(post) {
      return $http.put('/posts/' + post._id + '/upvote')
        .success(function(data){
          post.upvotes += 1;
        });
    };
    o.comment=function(postId, comment) {
    	console.log("creating comment", postId, comment);
      return $http.post('/posts/'+postId+'/comments', comment).success(function(data){
      	console.log("successsly created comment");
        o.posts.push(data);
      })
      .error(function(data, status, headers, config) {
	     // called asynchronously if an error occurs
	     // or server returns response with an error status.
	     alert("Error");
	     console.log("ERROR saving post", data, status, headers);
	    });
    }
	return o;
}]);

app.controller('PostsController', [
	'$scope',
	'$stateParams',
	'posts',
	function($scope, $stateParams, posts){
		console.log( posts, $stateParams.id );
		$scope.post = posts[$stateParams.id];
	}
]);

app.controller('MainController', [
	'$scope',
	'posts',
	function($scope, posts){
		$scope.posts = posts.posts;
	  	$scope.test = 'Hello world!';
		console.log("posts", posts.posts);
		$scope.addPost = function() {
			console.log("creating post", $scope.headline, $scope.body, new Date().getTime());
			if(!$scope.headline || $scope.headline === '') { return; }
			posts.create({
              headline: $scope.headline,
              body: $scope.body,
              created_at: new Date().getTime(),
              author: "Me"
            });

			$scope.link='';
			$scope.title = '';
		};
		$scope.addComment = function(post) {
			console.log("adding", post.comment, "to post", post);
			if(post.comment === '') { return; }
			if( !post.comments ) {
				post.comments=[];
			}
			var newComment={
		    	body: post.comment,
		    	author: 'user',
		    	upvotes: 0
		  	};
			post.comments.push(newComment);
		  	posts.comment(post._id, newComment);
		};
		$scope.incrementUpvotes = function(post) {
			posts.upvote(post);
		};
	}]);
